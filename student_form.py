#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Lauréline Pierre"

from Teachers import *
from Students import *


class Student_form:
    """
  Here you get get the name, family name ans mail of a given student
  """

    """ ATTRIBUTES

  Name  (private)

  Family_name  (private)

  Mail  (private)

  """

    def __init__(self, name, family_name, mail):
        self.name= name
        self.family_name = family_name
        self.mail = mail

    def set_name(self):
      self.name = name

    def get_name(self):
      return self.name


    def set_family_name(self):
      self.family_name = family_name

    def get_family_name(self):
      return self.family_name

    def set_mail(self):
      self.mail = mail

    def get_Mail(self):
      return self.mail

    def __str__(self):
      result = "The student name and family name is " + self.name + self.family_name + "and his/her mail is " + self.mail
