#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Lauréline Pierre"

from Teachers import *
from Students import *


class Teachers(object):

  """
   Here you can have access to all information concerning teachers
  """

  """ ATTRIBUTES

  Family_name  (private)

  Name  (private)

  Number  (private)

  Mail  (private)

  Entry_into_function  (private)

  Salary  (private)

  Class  (private)

  """

  def __init__(self, name, family_name, mail, number, entry_into_function, salary,subject):
    self.name = name
    self.family_name = family_name
    self.mail = mail
    self.number = number
    self.entry_into_function = entry_into_function
    self.salary = salary
    self.subject = subject

  def set_name(self):
    self.name = name

  def get_name(self):
    return self.name

  def set_family_name(self):
    self.family_name = family_name

  def get_family_name(self):
    return self.family_name

  def set_mail(self):
    self.mail = mail

  def get_Mail(self):
    return self.mail

  def set_number(self):
    self.number = number

  def get_number(self):
    return self.number

  def set_entry_into_function(self):
    self.set_entry_into_function

  def get_entry_into_function(self):
    return self.set_entry_into_function

  def set_salary(self):
    self.salary = salary

  def get_Salary(self):
    return  self.salary

  def set_subject(self):
    self.subject = subject

  def get_subject(self):
    return self.subject

  def __str__(self):
    result = "The informations concerning the teacher are " + "name: " +self.name + "family name:" + self.family_name + "number: "+ self.number + "mail: " + self.mail + "entry into function: " +  self.entry_into_function + "salary: "+ self.salary + " his/her subject: " +self.subject



