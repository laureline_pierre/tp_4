#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Lauréline Pierre"

from Teachers import *
from Students import *

class Students(object):

  """
  Here you can have access toa all information concerning students
  """

  """ ATTRIBUTES

  Family_name_  (private)

  Name  (private)

  Class  (private)

  Mail  (private)

  Number  (private)

  Entry_at_university  (private)

  class_notation  (private)

  Class_absence  (private)

  """

  def __init__(self, name, family_name, mail, number, subject, subject_notation, subject_absence,entry_at_university):
    self.name = name
    self.family_name = family_name
    self.mail = mail
    self.number = number
    self.subject = subject
    self.subject_notation = subject_notation
    self.subject_absence = subject_absence
    self.set_entry_at_university = entry_at_university

  def set_name(self):
    self.name = name

  def get_name(self):
    return self.name

  def set_family_name(self):
    self.family_name = family_name

  def get_family_name(self):
    return self.family_name

  def set_mail(self):
    self.mail = mail

  def get_mail(self):
    return self.mail

  def set_number(self):
    self.number = number

  def get_number(self):
    return self.number

  def set_entry_at_university(self):
    self.entry_at_university = entry_at_university

  def get_entry_at_university(self):
    return self.set_entry_at_university

  def set_subject(self):
    self.subject = subject

  def get_subject(self):
    return  self.subject

  def set_subject_notation(self):
    self.subject_notation = subject_notation

  def get_subject_notation(self):
    return self.subject_notation

  def set_subject_absence(self):
    self.subject_absence = subject_absence

  def __str__(self):
    result = "The informations concerning the teacher are " + "name: " +self.name + "family name:" + self.family_name + "number: "+ self.number + "mail: " + self.mail + "entry at university : " +  self.entry_at_university + "subject: "+ self.subject + "subject notation: " +self.subject_notation



