#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Lauréline Pierre"

from Teachers import *
from Students import *


class Department :

  """
  Thre university is composed of different departement given a unqiue class with a manager
  """

  """ ATTRIBUTES

  speciality_instruction  (private)

  Manager  (private)

  """

  def __init__(self, speciality_instruction, manager):
    self.speciality_instruction = speciality_instruction
      self.manager = manager

  def set_speciality_instruction (self)
     self.speciality_instruction = speciality_instruction

  def get_speciality_instruction(self):
    return self.speciality_instruction


  def __str__(self):
    result = "The speciality instruction is  " + self.speciality_instruction"and the manager of this department is " + self.manager
