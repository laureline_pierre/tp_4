#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Lauréline Pierre"

from Teachers import *
from Students import *

class Subject:

  """
  For each class, have acces to the teacher and the mean of this class
  """

  """ ATTRIBUTES

  Teacher_s_  (private)

  Mean_class  (private)

  """
  def __init__(self, Teacher_s,Mean_class, id_classroom):
    self.Teacher_s = Teacher_s
    self.Mean_class = Mean_class

  def set_Teacher_s_(self):
     self.Teacher_s = Teacher_s

  def get_Teacher_s_(self):
    return self.Teacher_s

  def set_Mean_class(self):
     self.Mean_class= Mean_class

  def get_Mean_class(self):
    return self.Mean_class


  def set_id_classroom(self):
    self.id_classroom = id_classroom

  def get_id_classroom(self):
    return self.id_classroom


  def __int__(self):
    result = "The website teacher is " + self.website "and the meean of his/her class is " + self.Mean_class
